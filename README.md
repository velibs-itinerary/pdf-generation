<p style="text-align:center!important">
<img src="https://pngimg.com/uploads/bicycle/bicycle_PNG5374.png" width="80px">
</p>
<h1 style="text-align:center!important">Velibs Itinerary - PDF</h1>

<div style="text-align:center!important">

[![Python](https://img.shields.io/badge/Python-3.11.4-blue?style=for-the-badge)](https://www.python.org/downloads/release/python-3114/)
[![Docker](https://img.shields.io/badge/docker-24.0.2-0DB7ED.svg?style=for-the-badge&logo=docker)](https://docker.com/)
[![Docker Compose](https://img.shields.io/badge/docker_compose-2.19.1-384D54.svg?style=for-the-badge&logo=docker)](https://docker.com/)
[![Postgres](https://img.shields.io/badge/postgresql-15.3-336791.svg?style=for-the-badge&logo=postgresql)](https://www.postgresql.org/)

</div>

---

<h2 id="table-des-matières">📝 Table des matières</h2>

- <a href="#table-des-matières">📝 Table des matières</a>
- <a href="#presentation-du-projet">📚 Présentation du projet</a>
- <a href="#setup-le-projet">⚙️ Setup le projet</a>
- <a href="#passer-a-sqlite">🔀 Passer à SQLite, sans docker</a>
- <a href="#explication-bref">🔨 Explication bref des methodes codées</a>
- <a href="#participants">👨‍👦‍👦 Participants</a>

<h2 id="presentation-du-projet">📚 Présentation du projet</h2>

Le projet Velibs Itinerary à été développé pour le module `API et Services web` proposé par `Livecampus`.

Ici le projet visait le système de génération de pdf. Il était demandé de fournir les méthodes suivantes :

Base: 
- Création d'un itinéraire
- Génération PDF de l'itinéraire
- Vérification de l'authentification de l'utilisateur via l'api authentification

Extras:
- Récupération des Itinéraires
- Récupération des PDF

Nous avons décidé d'utiliser django afin de pouvoir générer des pdf facilement et d'explorer l'utilisation de WeasyPrint (une bibliothèque qui génère des PDF en se basant sur le templating Django) dans un but éducatif.

<h2 id="setup-le-projet">⚙️ Setup le projet</h2>

Si cette étape ne fonctionne pas à un moment donné du lancement, veuillez vous référer à [🔀 Passer à SQLite, sans docker](#🔀-passer-à-sqlite-sans-docker).

Toutes les commandes ci-dessous sont spécifiées dans le fichier run.py (à la racine du projet).

**Lancement du projet**

- Copier le `.env.sample` en `.env` (dans /docker), ne modifier son contenu que si nécessaire
- Lancer le projet et la base de données (à la racine) : `python run.py up`
- Stop le projet et la base de données (à la racine) : `python run.py stop`

**Arrêt du projet**

- Couper le docker : ⌨️ `ctrl+c`
- Supprimer le docker : `python run.py down`

<h2 id="passer-a-sqlite">🔀 Passer à SQLite, sans docker</h2>

Si le docker ne fonctionne pas, il reste la solution de repasser à `SQLite`, sans docker. Pour ça il suffit de lancer les commandes ci-dessous.

- [DISCLAIMER WINDOWS pour l'installation des dépendances](#⚠️-disclaimer)

- Il est fortement recommandé d'installer PyEnv afin de pouvoir se mettre sur la version 3.11.4 de python ([https://github.com/pyenv/pyenv](https://github.com/pyenv/pyenv)) sinon il faudra une version de python similaire.

```
$ pyenv install 3.11.4
$ pyenv global 3.11.4
$ python --version
Python 3.11.4

Note: python3 ou python selon l'allas
```

- Il est également fortement recommandé d'utiliser un environnement virtuel (commande à exécuter à la racine du projet)

```
$ python -m venv env
Linux :
$ source env/bin/activate 
Windows: 
$ \env\Scripts\activate.bat

Note: python3 ou python selon l'allas
```

- Installer les dépendances:

```
pip install -r requirements.txt
```

#### ⚠️ DISCLAIMER

- L'installation de weasyPrint necessite une installation sur Windows 11 et n'est pas supporté par Windows 10: [https://doc.courtbouillon.org/weasyprint/stable/first_steps.html#windows](https://doc.courtbouillon.org/weasyprint/stable/first_steps.html#windows)


- Lancer l'application : 

```
$ python manage.py runserver 127.0.0.1:8002
```

<h2 id="explication-bref">🔨 Explication bref des methodes codées</h2>

Base: 
- Création d'un itinéraire : Sauvegarde du nom, de l'identifiant de l'utilisateur et du document PDF généré. Le document contient plusieurs points de passage (latitude, longitude) ainsi que son chemin relatif. 
- Génération du PDF de l'itinéraire : Génération d'un PDF à l'aide de WeasyPrint.
- Vérification de l'authentification de l'utilisateur via l'API d'authentification : Gérée par un middleware qui vérifie l'authentification de l'utilisateur sur chaque route en utilisant le package (request) et en interrogeant le serveur d'authentification.
- Vérification du corps de la requête : Gérée par un middleware et configurable pour chaque route dans /pdf/config/constants.json. Ce fichier contient la déclaration de chaque route en tant que clé et les champs obligatoires correspondants en tant que valeur.

Extras:
- Récupération des itinéraires : Effectue une requête en utilisant le jeton d'authentification de l'utilisateur et renvoie les résultats au front-end.
- Récupération des PDF : Étant donné que les PDF sont stockés, il est possible de récupérer les PDF.

MPD:

<div style="text-align:center!important">
    <img src="./docs/mpd.png">
</div>

<h2 id="participants">👨‍👦‍👦 Participants</h2>

**Mickaël Lebas**, **Alexis Py**, **Théo Posty**
