#!/bin/bash
set -e
export PGPASSWORD=$POSTGRES_PASSWORD;

psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  DO
  \$\$
  BEGIN
    IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'postgres_test') THEN
      CREATE USER postgres_test WITH PASSWORD '';
    END IF;
  END
  \$\$;
EOSQL

psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE DATABASE $POSTGRES_DB;
  GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;
  COMMIT;
EOSQL
