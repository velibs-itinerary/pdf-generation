#!/bin/bash

ID="0.0.0.0"
PORT=8000

echo "Starting migrations..."
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput

# Démarrage du serveur Django
echo "Starting django serv..."

python manage.py runserver $ID:$PORT