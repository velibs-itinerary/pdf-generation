import subprocess


def get_docker_ip():
    try:
        # Exécute la commande shell pour obtenir l'adresse IP de docker0
        command = "ip addr show docker0 | grep -Po 'inet \K[\d.]+'"
        output = subprocess.check_output(command, shell=True).decode().strip()
        return output
    except subprocess.CalledProcessError:
        return None


# Récupère l'adresse IP de docker0
docker_ip = get_docker_ip()

if docker_ip:
    # Enregistre l'adresse IP dans le fichier .env
    with open(".env", "a") as env_file:
        env_file.write(f"HOST_IP={docker_ip}\n")
else:
    print("Unable to recover the IP address of the Docker interface")
