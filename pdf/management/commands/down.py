import os
import platform
import subprocess

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = ""
    help = "Run the docker container"

    root_directory = settings.BASE_DIR
    docker_compose_dir = os.path.join(root_directory, "docker")
    command = "docker compose down"

    def handle(self, *args, **options):
        try:
            if platform.system() == "Linux":
                # Linux
                subprocess.run(
                    f"cd {self.docker_compose_dir} && sudo {self.command}",
                    shell=True,
                    check=True,
                )
            elif platform.system() == "Windows":
                # Windows
                subprocess.run(
                    f"cd /d {self.docker_compose_dir} && {self.command}",
                    shell=True,
                    check=True,
                )
            else:
                return print("Unsupported operating system.")

        except subprocess.CalledProcessError as e:
            return print(f"Error starting container: {str(e)}")
