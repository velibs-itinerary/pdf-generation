import json

import requests
from django.conf import settings
from django.http import JsonResponse


class AuthTokenMiddleware:
    """
    Middleware that check the token throw the auth api
    with post request

    Add the user to request if all is ok
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # token should be in request.COOKIES ?
        if request.__dict__["method"] not in ("GET", "OPTIONS"):
            request_data = json.loads(request.body)
            if "token" in request_data:
                try:
                    token = request_data["token"]
                    response_data = verify_auth_token(token)
                    # setting user to request
                    request.user = response_data
                except ValueError as e:
                    # failsafe
                    request.user = None
                    # Return the error response
                    return JsonResponse(e.args[0], status=e.args[0]["status"])
            else:
                # failsafe
                request.user = None

        response = self.get_response(request)
        return response


def verify_auth_token(token):
    """
    Request the auth API with the given token
    to check if the user is connected.

    return user if success else json error
    """
    url = settings.AUTH_API_URL
    data = {"token": token}
    headers = {
        "Content-Type": "application/json",
    }

    try:
        response = requests.post(url, data=json.dumps(data), headers=headers)
        user_response = response.json()
        if response.status_code == 200:
            # retrieving response data as JSON
            return user_response
        else:
            # raise a ValueError with the response JSON as the error message
            raise ValueError(user_response)

    except requests.exceptions.RequestException as e:
        print("An error occurred:", e)
        # raise a ValueError with the error message
        raise ValueError(
            {
                "message": "An error occurred during token verification",
                "status": 400,
            }
        )
