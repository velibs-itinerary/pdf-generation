import json
import os

from django.http import JsonResponse
from django.urls import resolve


class CheckJsonBodyMiddleware:
    """
    Middleware that check the body request contains the right json fields
    In config > constants.json
    The json based in the path url
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.__dict__["method"] not in ("GET", "OPTIONS"):
            request_data = json.loads(request.body)
            try:
                path_name = resolve(request.path_info).url_name
                self.test_json_body(route_path=path_name, data=request_data)
            except ValueError as e:
                # Return the error response
                return JsonResponse(e.args[0], status=e.args[0]["status"])

        response = self.get_response(request)
        return response

    def test_json_body(self, route_path, data):
        """
        Test the body of the request

        return error if one of the asserts not passed
        """

        # Path to the JSON file rules
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        file_path = os.path.join(base_dir, "config", "constants.json")

        # Load the contents of the JSON file
        with open(file_path, "r") as file:
            constants = json.load(file)

        # Check if each key exists and has a value
        for key in constants[route_path]:
            if key not in data or data[key] is None:
                raise ValueError(
                    {"message": f"Incorrect JSON {key}", "status": 400}
                )
