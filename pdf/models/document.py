from django.db import models


class Document(models.Model):
    """
    Represents pdf file
    """

    name = models.CharField(max_length=255)
    file = models.FileField(upload_to="documents/")

    class Meta:
        verbose_name = "Document"
        verbose_name_plural = "Documents"
