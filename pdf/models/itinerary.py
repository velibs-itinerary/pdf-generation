from django.db import models

from .document import Document


class Itinerary(models.Model):
    """
    Represents an itinerary of user
    """

    name = models.CharField(max_length=255)
    user_id = models.IntegerField(verbose_name="user id")
    # TODO Should be null when the queue system is up
    document = models.OneToOneField(Document, on_delete=models.CASCADE)
