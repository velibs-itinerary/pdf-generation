from django.contrib.gis.db import models

from .itinerary import Itinerary


class Waypoint(models.Model):
    """
    Represents a coordinates of a point
    """

    name = models.CharField(max_length=255)
    # In a perfect world PointField will be better but needed gdal installation # noqa
    # coordinates = models.PointField()
    longitude = models.CharField(max_length=20)
    latitude = models.CharField(max_length=20)
    itinerary = models.ForeignKey(
        Itinerary, on_delete=models.CASCADE, related_name="waypoints"
    )

    class Meta:
        verbose_name = "Waypoint"
        verbose_name_plural = "Waypoints"
