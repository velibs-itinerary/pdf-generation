from django import template

register = template.Library()


@register.filter
def seconds_to_hours(seconds):
    hours = seconds // 3600  # 1 hour = 3600 seconds
    minutes = (seconds % 3600) // 60  # 1 minute = 60 seconds

    if hours == 0:
        return f"{minutes} minutes"
    else:
        return f"{hours} h {minutes} m"


@register.filter
def convert_to_km(value):
    kilometers = value / 1000
    return f"{kilometers:.2f} km"


@register.filter
def step_icon_path(step):
    step_type = step["type"]

    AVAILABLE_ICONS = [
        "Straight",
        "SlightRight",
        "Right",
        "SharpRight",
        "TurnAround",
        "Uturn",
        "SharpLeft",
        "Left",
        "SlightLeft",
        "Waypoint",
        "Roundabout",
        "EnterAgainstAllowedDirection",
        "LeaveAgainstAllowedDirection",
        "Continue",
        "EndOfRoad",
        "Fork",
    ]

    if step_type not in AVAILABLE_ICONS:
        return "Default.svg"

    if step_type == "EndOfRoad":
        if step["text"].startswith("Turn left"):
            step_type = "Left"
        if step["text"].startswith("Turn slight left"):
            step_type = "SlightLeft"
        if step["text"].startswith("Turn sharp left"):
            step_type = "SharpLeft"
        if step["text"].startswith("Turn right"):
            step_type = "Right"
        if step["text"].startswith("Turn slight right"):
            step_type = "SlightRight"
        if step["text"].startswith("Turn sharp right"):
            step_type = "SharpRight"


    if step_type == "Fork":
        if step["text"].startswith("Keep left"):
            step_type = "Left"
        if step["text"].startswith("Keep right"):
            step_type = "Right"
        if step["text"].startswith("Keep straight"):
            step_type = "Straight"

    path = "icons/" + step_type + ".svg"

    return path


@register.filter
def step_icon_text(step):
    import re

    AVAILABLE_ICONS = [
        "Straight",
        "SlightRight",
        "Right",
        "SharpRight",
        "TurnAround",
        "Uturn",
        "SharpLeft",
        "Left",
        "SlightLeft",
        "Waypoint",
        "Roundabout",
        "EnterAgainstAllowedDirection",
        "LeaveAgainstAllowedDirection",
        "Continue",
        "EndOfRoad",
        "Fork",
    ]

    if step not in AVAILABLE_ICONS:
        return "Default Image"

    return re.sub(r"(?<=\w)([A-Z])", r" \1", step) + " Image"
