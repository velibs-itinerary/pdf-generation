from django.urls import path

from .views.itinerary import (
    ItineraryManager,
    ItineraryPatch,
    PdfDowloaderManager,
    delete_itinerary,
    get_itinerary,
)

urlpatterns = [
    path("itinerary/", ItineraryManager.as_view(), name="create_itinerary"),
    path(
        "itinerary/<int:id>/", ItineraryPatch.as_view(), name="edit_itinerary"
    ),
    path(
        "itinerary/delete/<int:id>/",
        delete_itinerary,
        name="delete_itinerary",
    ),
    path(
        "itineraries/",
        get_itinerary,
        name="get_itinerary",
    ),
    path(
        "itinerary/download/",
        PdfDowloaderManager.as_view(),
        name="download_pdf",
    ),
]
