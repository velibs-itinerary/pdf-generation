import io
import json
import os
import uuid

from django.conf import settings
from django.core.files.base import File
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from django.views.generic import View
from weasyprint import HTML

from ..models.document import Document
from ..models.itinerary import Itinerary
from ..models.waypoint import Waypoint


class ItineraryManager(View):
    pdf_template = "default_template_itinerary.html"

    def post(self, request):
        """
        Create a pdf and save it into database
        """
        # request.body is a string, need to load it into json
        response_data = json.loads(request.body)
        try:
            self.save_itinerary(context=response_data)

            response = HttpResponse(status=204)
            # Just return 204 if everything is ok
            return response

        except ValueError as e:
            # Return the error response
            return JsonResponse(e.args[0], status=e.args[0]["status"])

    def save_itinerary(self, context):
        """
        Create into database :
            - document
            - itinerary
            - waypoints
        """
        try:
            # Generate random filename
            filename = str(uuid.uuid4())

            # Generate a PDF
            pdf_file = self.generate_pdf(
                # return self.generate_pdf(
                template_name=self.pdf_template,
                filename=filename,
                context={
                    **context,
                    "user": self.request.user,
                    "from_destination": context["steps"][0],
                    "to_destination": context["steps"][-1],
                },
            )

            # Get all waypoints from the context
            waypoints = [
                step
                for step in context["steps"]
                if step["type"] == "Waypoint"
            ]

            # Create the itinerary
            itinerary = Itinerary.objects.create(
                name=context["name"],
                user_id=self.request.user["id"],
                document=Document.objects.create(
                    name=filename, file=pdf_file
                ),
            )

            # Create all waypoint and add the itinerary to them
            waypoint_instances = [
                Waypoint(
                    name=waypoint["text"],
                    longitude=waypoint["lng"],
                    latitude=waypoint["lat"],
                    itinerary=itinerary,
                )
                for waypoint in waypoints
            ]

            # Bulk create the waypoints
            Waypoint.objects.bulk_create(waypoint_instances)

        except ValueError as e:
            # Return the error response
            return JsonResponse(e.args[0], status=e.args[0]["status"])

    def generate_pdf(self, template_name, filename, context):
        """
        Generate a pdf

        template_name: path + name of the html template
        filename: name of the generated pdf
        context: extra args passed to template

        return django file
        """
        pdf_file = io.BytesIO()

        # pass request to use context_processors
        rendered = render_to_string(
            template_name, context, request=self.request
        )

        doc = HTML(
            encoding="utf-8",
            string=rendered,
            base_url=self.request.build_absolute_uri(),
        )
        # Render into html
        doc = doc.render()
        # doc.metadata.title = request.body.name
        # doc.metadata.authors = request.body.username
        doc.write_pdf(pdf_file)

        pdf_file.seek(0)
        # debug
        # return FileResponse(pdf_file, filename=filename + ".pdf")
        return File(pdf_file, name=f"{filename}.pdf")


class PdfDowloaderManager(View):
    def post(self, request):
        """
        Download the pdf
        """
        response_data = json.loads(request.body)

        try:
            pdf_file = Document.objects.get(id=response_data["id"])

            # checks if the itinerary has the same user_id as the token id
            if pdf_file.itinerary.user_id != request.user["id"]:
                raise ValueError({"message": "Not allowed", "status": 400})

            file_name = os.path.join(
                settings.MEDIA_ROOT + "/documents/" + pdf_file.name + ".pdf"
            )

            if os.path.exists(file_name):
                with open(file_name, "rb") as file:
                    response = HttpResponse(
                        file.read(), content_type="application/pdf"
                    )
                    response[
                        "Content-Disposition"
                    ] = f'attachment; filename="{pdf_file.name}"'
                    return response
            else:
                raise ValueError(
                    {"message": "Document not found", "status": 400}
                )

        except (ValueError, Document.DoesNotExist) as e:
            # Return the error response
            if type(e).__name__ == "DoesNotExist":
                return JsonResponse({"message": "Not found"}, status=400)
            return JsonResponse(e.args[0], status=e.args[0]["status"])


def get_itinerary(request):
    """
    Get all itineraries of a user
    """
    try:
        itinerary_queryset = Itinerary.objects.filter(
            user_id=request.user["id"]
        ).prefetch_related("document")

        data = []
        for itinerary in itinerary_queryset:
            # load every waypoint of the queryset
            waypoints_queryset = itinerary.waypoints.all()

            # serialize
            waypoints = []
            for waypoint in waypoints_queryset:
                wp_data = {
                    "name": waypoint.name,
                    "longitude": waypoint.longitude,
                    "latitude": waypoint.latitude,
                }

                waypoints.append(wp_data)

            # access the related document field
            # serialize
            itinerary_data = {
                "id": itinerary.id,
                "name": itinerary.name,
                "user_id": itinerary.user_id,
                "document": {
                    "id": itinerary.document.id
                    if itinerary.document
                    else None,
                    "name": itinerary.document.name
                    if itinerary.document
                    else None,
                },
                "waypoints": waypoints,
            }
            data.append(itinerary_data)

        return JsonResponse(data, safe=False)

    except (ValueError, Itinerary.DoesNotExist) as e:
        # Return the error response
        if type(e).__name__ == "DoesNotExist":
            return JsonResponse({"message": "Not found"}, status=400)
        return JsonResponse(e.args[0], status=e.args[0]["status"])


class ItineraryPatch(ItineraryManager):
    def post(self, request):
        pass

    def get(self, request):
        pass

    def put(self, request, id):
        """
        Create a pdf and save it into database
        """
        response_data = json.loads(request.body)
        try:
            self.put_itinerary(context=response_data, id=id)

            response = HttpResponse(status=204)
            # Just return 204 if everything is ok
            return response

        except (
            ValueError,
            Itinerary.DoesNotExist,
            Waypoint.DoesNotExist,
            Document.DoesNotExist,
        ) as e:
            # Return the error response
            if type(e).__name__ == "DoesNotExist":
                return JsonResponse({"message": "Not found"}, status=400)
            return JsonResponse(e.args[0], status=e.args[0]["status"])

    def put_itinerary(self, context, id):
        """
        Create into database :
            - document
            - itinerary
            - waypoints
        """

        try:
            # Generate random filename
            filename = str(uuid.uuid4())

            # Generate a PDF
            pdf_file = self.generate_pdf(
                # return self.generate_pdf(
                template_name=self.pdf_template,
                filename=filename,
                context={
                    **context,
                    "user": self.request.user,
                    "from_destination": context["steps"][0],
                    "to_destination": context["steps"][-1],
                },
            )

            # Get all waypoints from the context
            waypoints = [
                step
                for step in context["steps"]
                if step["type"] == "Waypoint"
            ]

            # Search itinerary by id

            itinerary_data = Itinerary.objects.filter(id=id)[0]

            old_file_name = itinerary_data.document.name + ".pdf"

            # remove old pdf file
            path = os.path.join(
                settings.BASE_DIR, "media/documents/" + old_file_name
            )
            if os.path.exists(path):
                os.remove(path)

            itinerary_data.name = context["name"]

            itinerary_data.document.delete()
            itinerary_data.waypoints.all().delete()

            new_doc_intance = Document.objects.create(
                name=filename, file=pdf_file
            )

            itinerary_data.document = new_doc_intance

            itinerary_data.save()

            # Create all waypoint and add the itinerary to them
            waypoint_instances = [
                Waypoint(
                    name=waypoint["text"],
                    longitude=waypoint["lng"],
                    latitude=waypoint["lat"],
                    itinerary=itinerary_data,
                )
                for waypoint in waypoints
            ]

            # Bulk create the waypoints
            Waypoint.objects.bulk_create(waypoint_instances)

        except (
            ValueError,
            Itinerary.DoesNotExist,
            Waypoint.DoesNotExist,
            Document.DoesNotExist,
        ) as e:
            # Return the error response
            if type(e).__name__ == "DoesNotExist":
                return JsonResponse({"message": "Not found"}, status=400)
            return JsonResponse(e.args[0], status=e.args[0]["status"])


def delete_itinerary(request, id):
    """
    delete all related to pdf
    """
    try:
        itinerary_queryset = Itinerary.objects.get(id=id)
        itinerary_queryset.document.name
        file_name = itinerary_queryset.document.name + ".pdf"

        # remove pdf file
        path = os.path.join(settings.BASE_DIR, "media/documents/" + file_name)
        if os.path.exists(path):
            os.remove(path)
            itinerary_queryset.delete()

        response = HttpResponse(status=204)
        # Just return 204 if everything is ok
        return response

    except (ValueError, Itinerary.DoesNotExist) as e:
        # Return the error response
        if type(e).__name__ == "DoesNotExist":
            return JsonResponse({"message": "Not found"}, status=400)
        return JsonResponse(e.args[0], status=e.args[0]["status"])
