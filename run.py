import os
import platform
import subprocess
import sys


class DockerCommands:
    """
    Command tools to start the project with docker
    No needed django just python
    """

    docker_compose_dir = os.path.join("docker")

    def run_up(self):
        command = "docker compose up --build"
        self._execute_command(command)

    def run_down(self):
        command = "docker compose down"
        self._execute_command(command)

    def run_stop(self):
        command = "docker compose stop"
        self._execute_command(command)

    def _execute_command(self, command):
        try:
            if platform.system() == "Linux":
                subprocess.run(
                    f"cd {self.docker_compose_dir} && sudo {command}",
                    shell=True,
                    check=True,
                )
            elif platform.system() == "Windows":
                subprocess.run(
                    f"cd /d {self.docker_compose_dir} && {command}",
                    shell=True,
                    check=True,
                )
            else:
                print("Unsupported operating system.")
        except subprocess.CalledProcessError as e:
            print(f"Error executing command: {str(e)}")


# Usage
docker_commands = DockerCommands()
if len(sys.argv) > 1:
    command = sys.argv[1]
    if command == "up":
        docker_commands.run_up()
    elif command == "down":
        docker_commands.run_down()
    elif command == "stop":
        docker_commands.run_stop()
    else:
        print("Invalid command. ('up', 'stop' or 'down').")
else:
    print("Please provide a command ('up', 'stop' or 'down').")
